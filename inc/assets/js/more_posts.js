(function ($) {
    "use strict";

    $(document).on('click', '.btn-load-more-posts', function () {

        var that = $(this);
        var page = $(this).data('page');
        var newPage = page + 1;
        var ajaxurl = that.data('url');

        that.addClass('loading').find('.text').fadeOut(100);
        that.find('.icon1').fadeOut(100);
        that.find('.icon2').fadeIn(500).addClass('spin');

        $.ajax({

            url: ajaxurl,
            type: 'post',
            data: {
                page: page,
                action: 'msnoch_load_more_posts_for_news'
            },
            error: function (response) {
                console.log(response);
            },
            success: function (response) {

                if (response == 0) {
                    that.fadeOut(320);
                } else {
                    that.data('page', newPage);
                    $('.more-posts-deck').append(response);
                    that.removeClass('loading').find('.text').fadeIn(100);
//                    that.find('.icon2').fadeOut(500).removeClass('spin');
                }
            }

        });

    });

})(jQuery);
