<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
get_header('secondary');
$cat_id = get_query_var('cat');
?>
<section class="slider">
    <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
            <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
            <div class="carousel-item active">
                <?php $slide_image_1 = get_field('slide_image_1'); ?>
                <img src="<?php echo $slide_image_1['url'] ?>" alt="<?php echo $slide_image_1['alt']; ?>" />
                <div class="carousel-caption">
                    <h5><?php echo the_field('slide_title_1'); ?></h5>
                    <p><?php echo the_field('slide_description_1'); ?></p>
                    <button class="btn btn-outline-primary"><span class="txt">Lorem</span></button>
                </div>
            </div>
            <div class="carousel-item">
                <?php $slide_image_2 = get_field('slide_image_2'); ?>
                <img src="<?php echo $slide_image_2['url'] ?>" alt="<?php echo $slide_image_2['alt']; ?>" />
                <div class="carousel-caption">
                    <h5><?php echo the_field('slide_title_2'); ?></h5>
                    <p><?php echo the_field('slide_description_2'); ?></p>
                    <button class="btn btn-outline-primary"><span class="txt">Lorem</span></button>
                </div>
            </div>
            <div class="carousel-item">
                <?php $slide_image_3 = get_field('slide_image_3'); ?>
                <img src="<?php echo $slide_image_3['url'] ?>" alt="<?php echo $slide_image_3['alt']; ?>" />
                <div class="carousel-caption">
                    <h5><?php echo the_field('slide_title_3'); ?></h5>
                    <p><?php echo the_field('slide_description_3'); ?></p>
                    <button class="btn btn-outline-primary"><span class="txt">Lorem</span></button>
                </div>
            </div>
        </div>
    </div>
    <div class="slider-icons">
        <div class="slider-social-icons">
            <?php if (get_field('instagram')) : ?>
                <a href="<?php the_field('instagram'); ?>" target="__blank"><i class="fa fa-instagram"></i></a>
            <?php endif; ?>
            <?php if (get_field('twitter')) : ?>
                <a href="<?php the_field('twitter'); ?>" target="__blank"><i class="fa fa-twitter"></i></a>
            <?php endif; ?>
            <?php if (get_field('facebook')) : ?>
                <a href="<?php the_field('facebook'); ?>" target="__blank"><i class="fa fa-facebook"></i></a>
            <?php endif; ?>
            <?php if (get_field('www')) : ?>
                <a href="<?php the_field('www'); ?>" target="__blank"><i class="fa fa-globe"></i></a>
            <?php endif; ?>
        </div>
        <div class="slider-arrow-down">
            <a href="#about"><i class="fa fa-chevron-down"></i></a>
        </div>
    </div>
</section>
<section id="about" class="about">
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-5">
                <img class="img-fluid" src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group-20.png" alt="">
            </div>
            <div class="col-5 py-5">
                <h1>O nas</h1>
                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                    eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et
                    accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est
                    Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy
                    eirmod</p>
            </div>
        </div>
    </div>
</section>
<section id="offer" class="offer">
    <div class="container">
        <h1>OFERTA</h1>
        <div id="offerCarousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <div class="row">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                    </div>
                </div>
                <div class="carousel-item">
                    <div class="row">
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                        <div class="col">
                            <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Group 794.svg" alt="">
                            <h2>Lorem impsum</h2>
                        </div>
                    </div>
                </div>
            </div>
            <a class="carousel-control-prev" href="#offerCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#offerCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
</section>
<section id="news" class="news">
    <div class="container">
        <h1>AKTUALNOŚCI</h1>
        <div class="row more-posts-deck">
            <?php
            $args = array(
                'type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => 4,
                'category_name' => 'front-page',
                'ignore_sticky_posts' => 1,
            );

            $slice_query = new WP_Query($args);
            ?>

            <?php if ($slice_query->have_posts()): while ($slice_query->have_posts()): $slice_query->the_post(); ?>

                    <?php get_template_part('template-parts/content', 'news'); ?>

                <?php endwhile; ?>

            <?php else : ?>

                <?php get_template_part('template-parts/content', 'none'); ?>

            <?php endif; ?>

            <?php wp_reset_postdata(); ?>
        </div>
        <div class="load-more-posts-container py-5 text-center">
            <button class="btn btn-outline-primary btn-load-more-posts" data-page="1" data-url="<?php echo admin_url('admin-ajax.php') ?>">
                <span class="icon2">Pokaż więcej</span>
            </button>
        </div>
    </div>
</section>
<footer class="page-footer">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="footer-logo">
                        <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/GrupaVIST-01biale@2x.png" alt="" width="134" height="108">
                    </div>
                    <div class="footer-phone">
                        <img src="<?php echo get_template_directory_uri() ?>/inc/assets/img/Icon material-phone-in-talk.svg" alt="" width="134" height="108">
                        <p><span>ZADZWOŃ</span><br>693 097 703</p>
                    </div>
                </div>
                <div class="col">
                    <h3>Kontakt</h3>
                    <address>
                        Ul. Słowackiego 33-37/8,<br>33-100 Tarnów<br>Ul. Kościerzyńska 1A,<br>51-416 Wrocław<br>Tel: +48 693 097 703<br> Tel: +48 690 657 203<br>E-Mail: biuro@advist.pl
                    </address>
                </div>
                <div class="col">
                    <h3>Kontakt</h3>
                    <address>
                        Ul. Słowackiego 33-37/8,<br>33-100 Tarnów<br>Ul. Kościerzyńska 1A,<br>51-416 Wrocław<br>Tel: +48 693 097 703<br> Tel: +48 690 657 203<br>E-Mail: biuro@advist.pl
                    </address>
                </div>
                <div class="col">
                    <h3>Kontakt</h3>
                    <address>
                        Ul. Słowackiego 33-37/8,<br>33-100 Tarnów<br>Ul. Kościerzyńska 1A,<br>51-416 Wrocław<br>Tel: +48 693 097 703<br> Tel: +48 690 657 203<br>E-Mail: biuro@advist.pl
                    </address>
                </div>
                <div class="col">
                    <h3>Kontakt</h3>
                    <address>
                        Ul. Słowackiego 33-37/8,<br>33-100 Tarnów<br>Ul. Kościerzyńska 1A,<br>51-416 Wrocław<br>Tel: +48 693 097 703<br> Tel: +48 690 657 203<br>E-Mail: biuro@advist.pl
                    </address>
                </div>
            </div>
        </div>
    </div>
    <div class="copyright">
        <div class="container text-center">
            © Copyright 2020 zadanie-rekrutacyjne-advist. All rights reserved.
        </div>
    </div>
</footer>
<?php
get_footer('secondary');
