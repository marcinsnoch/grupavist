# GULP Starter

Gulp, Sass, Browser Sync, Bootstrap 4, jQuery



Install & copy plugins

```bash
npm install
```

Build

```bash
npm run build
```

Dev

```bash
npm run dev
```
