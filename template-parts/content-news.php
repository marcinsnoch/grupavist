<?php
/**
 * Template part for displaying page content
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WP_Bootstrap_Starter
 */
?>

<div class="col-md-3">
    <div class="news-box">
        <?php the_post_thumbnail('medium_large', array('class' => 'img-fluid')); ?>
        <p><?php echo wp_trim_words( get_the_content(), 20, '...' ); ?></p>
        <a href="<?php echo get_permalink() ?>" class="btn btn-primary">Czytaj</a>
    </div>
</div>